// Reference: https://web.mit.edu/6.111/volume2/www/f2019/handouts/labs/lab1_19/
// Reference: http://classweb.ece.umd.edu/enee245.F2016/Lab8.pdf
module binary_to_seven_segment(hex_out, bin_in);

    input [3:0] bin_in;   
    output [7:0] hex_out;  
    
    assign hex_out = (bin_in == 4'b0000) ? 8'b10000001 : 8'bz; // 0
    assign hex_out = (bin_in == 4'b0001) ? 8'b11001111 : 8'bz; // 1
    assign hex_out = (bin_in == 4'b0010) ? 8'b10010010 : 8'bz; // 2
    assign hex_out = (bin_in == 4'b0011) ? 8'b10000110 : 8'bz; // 3
    assign hex_out = (bin_in == 4'b0100) ? 8'b11001100 : 8'bz; // 4
    assign hex_out = (bin_in == 4'b0101) ? 8'b10100100 : 8'bz; // 5
    assign hex_out = (bin_in == 4'b0110) ? 8'b10100000 : 8'bz; // 6
    assign hex_out = (bin_in == 4'b0111) ? 8'b10001111 : 8'bz; // 7
    assign hex_out = (bin_in == 4'b1000) ? 8'b10000000 : 8'bz; // 8
    assign hex_out = (bin_in == 4'b1001) ? 8'b10000100 : 8'bz; // 9
    assign hex_out = (bin_in == 4'b1010) ? 8'b10001000 : 8'bz; // A
    assign hex_out = (bin_in == 4'b1011) ? 8'b11100000 : 8'bz; // b
    assign hex_out = (bin_in == 4'b1100) ? 8'b10110001 : 8'bz; // C
    assign hex_out = (bin_in == 4'b1101) ? 8'b11000010 : 8'bz; // d
    assign hex_out = (bin_in == 4'b1110) ? 8'b10110000 : 8'bz; // E
    assign hex_out = (bin_in == 4'b1111) ? 8'b10111000 : 8'bz; // F

endmodule //binary_to_hex