module mux_2(out, select, in);
	input select;
	input [63:0] in;
	output [31:0] out;
	assign out = select ? in[63:32] : in[31:0];
endmodule
