module cla_block_8bit(s, G, P, x, y, g, p, cin);

    input [7:0] x, y, g, p;
    input cin;

    output [7:0] s;
    output G, P;

    and AND_P(P, p[7], p[6], p[5], p[4], p[3], p[2], p[1], p[0]);
    
    // Sum-of-products for G for the whole block.
    //region
    //
    //   (note that G + P * cin is equivalent to what would be c8, but 
    //    avoids using gates larger than 8.)

    wire G_sop_6, G_sop_5, G_sop_4, G_sop_3, G_sop_2, G_sop_1, G_sop_0;

    and AND_G6(G_sop_6, p[7], g[6]);
    and AND_G5(G_sop_5, p[7], p[6], g[5]);
    and AND_G4(G_sop_4, p[7], p[6], p[5], g[4]);
    and AND_G3(G_sop_3, p[7], p[6], p[5], p[4], g[3]);
    and AND_G2(G_sop_2, p[7], p[6], p[5], p[4], p[3], g[2]);
    and AND_G1(G_sop_1, p[7], p[6], p[5], p[4], p[3], p[2], g[1]);
    and AND_G0(G_sop_0, p[7], p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    
    //endregion
    or OR_G(G, g[7], G_sop_6, G_sop_5, G_sop_4, G_sop_3, G_sop_2, G_sop_1, G_sop_0);


    // CARRY SIGNALS: cX == c[X] is carry IN to full adder X.
    //
    //region
    //


    wire [7:0] c;
    assign c[0] = cin;
    
    //  -- c1

    wire c1_sop_0;

    and AND_C1_0(c1_sop_0, p[0], cin);
    or  OR_C1(c[1], g[0], c1_sop_0);

    //  -- c2

    wire c2_sop_1, c2_sop_0;

    and AND_C2_1(c2_sop_1, p[1], g[0]);
    and AND_C2_0(c2_sop_0, p[1], p[0], cin);
    or  OR_C2(c[2], g[1], c2_sop_1, c2_sop_0);

    //  -- c3

    wire c3_sop_2, c3_sop_1, c3_sop_0;

    and AND_C3_2(c3_sop_2, p[2], g[1]);
    and AND_C3_1(c3_sop_1, p[2], p[1], g[0]);
    and AND_C3_0(c3_sop_0, p[2], p[1], p[0], cin);
    or  OR_C3(c[3], g[2], c3_sop_2, c3_sop_1, c3_sop_0);

    //  -- c4

    wire c4_sop_3, c4_sop_2, c4_sop_1, c4_sop_0;

    and AND_C4_3(c4_sop_3, p[3], g[2]);
    and AND_C4_2(c4_sop_2, p[3], p[2], g[1]);
    and AND_C4_1(c4_sop_1, p[3], p[2], p[1], g[0]);
    and AND_C4_0(c4_sop_0, p[3], p[2], p[1], p[0], cin);
    or  OR_C4(c[4], g[3], c4_sop_3, c4_sop_2, c4_sop_1, c4_sop_0);

    //  -- c5

    wire c5_sop_4, c5_sop_3, c5_sop_2, c5_sop_1, c5_sop_0;

    and AND_C5_4(c5_sop_4, p[4], g[3]);
    and AND_C5_3(c5_sop_3, p[4], p[3], g[2]);
    and AND_C5_2(c5_sop_2, p[4], p[3], p[2], g[1]);
    and AND_C5_1(c5_sop_1, p[4], p[3], p[2], p[1], g[0]);
    and AND_C5_0(c5_sop_0, p[4], p[3], p[2], p[1], p[0], cin);
    or  OR_C5(c[5], g[4], c5_sop_4, c5_sop_3, c5_sop_2, c5_sop_1, c5_sop_0);

    //  -- c6

    wire c6_sop_5, c6_sop_4, c6_sop_3, c6_sop_2, c6_sop_1, c6_sop_0;

    and AND_C6_5(c6_sop_5, p[5], g[4]);
    and AND_C6_4(c6_sop_4, p[5], p[4], g[3]);
    and AND_C6_3(c6_sop_3, p[5], p[4], p[3], g[2]);
    and AND_C6_2(c6_sop_2, p[5], p[4], p[3], p[2], g[1]);
    and AND_C6_1(c6_sop_1, p[5], p[4], p[3], p[2], p[1], g[0]);
    and AND_C6_0(c6_sop_0, p[5], p[4], p[3], p[2], p[1], p[0], cin);
    or  OR_C6(c[6], g[5], c6_sop_5, c6_sop_4, c6_sop_3, c6_sop_2, c6_sop_1, 
        c6_sop_0);

    //  -- c6

    wire c7_sop_6, c7_sop_5, c7_sop_4, c7_sop_3, c7_sop_2, c7_sop_1, c7_sop_0;

    and AND_C7_6(c7_sop_6, p[6], g[5]);
    and AND_C7_5(c7_sop_5, p[6], p[5], g[4]);
    and AND_C7_4(c7_sop_4, p[6], p[5], p[4], g[3]);
    and AND_C7_3(c7_sop_3, p[6], p[5], p[4], p[3], g[2]);
    and AND_C7_2(c7_sop_2, p[6], p[5], p[4], p[3], p[2], g[1]);
    and AND_C7_1(c7_sop_1, p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    and AND_C7_0(c7_sop_0, p[6], p[5], p[4], p[3], p[2], p[1], p[0], cin);
    or  OR_C7(c[7], g[6], c7_sop_6, c7_sop_5, c7_sop_4, c7_sop_3, c7_sop_2, 
        c7_sop_1, c7_sop_0);

    //endregion
    
    // S, A, B, Cin
    full_adder FULL_ADD[7:0](s, x, y, c);

endmodule