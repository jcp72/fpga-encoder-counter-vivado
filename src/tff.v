module tff(q, toggle, clk, clr);

    // dffe_ref (q, d, clk, en, clr);
    // dffe that toggles 

    input clr, clk, toggle;
    output q;

    dffe_ref DFF(q, ~q, clk, toggle, clr);

endmodule