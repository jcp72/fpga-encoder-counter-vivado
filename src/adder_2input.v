module adder_2input(add_result, data_operandA, data_operandB, cin);

    input [31:0] data_operandA, data_operandB;
    input cin;
    output [31:0] add_result;

    // adder(add_result, data_operandA, data_operandB, and_result, or_result, cin)
    adder ADD(add_result, data_operandA, data_operandB, data_operandA & data_operandB, data_operandA | data_operandB, cin);

endmodule