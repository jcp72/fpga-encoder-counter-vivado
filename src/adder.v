module adder(add_result, data_operandA, data_operandB, and_result, 
        or_result, cin);

    input [31:0] data_operandA, data_operandB, and_result, or_result;
    input cin;

    output [31:0] add_result;

    wire [3:0] G;
    wire [3:0] P;

    wire [3:0] C; // Important note: capital C == [CC24, C16, C8, C0]
                  // therefore C[x] = C(8X)

    generate // 4x 8-bit CLA blocks
        genvar i;
        for (i = 0; i < 4; i = i + 1) begin : cla_gen
            // s, G, P, x, y, g, p, cin
            cla_block_8bit CLA(add_result[i * 8 + 7 : i * 8], G[i], P[i], 
                            data_operandA[i * 8 + 7 : i * 8], 
                            data_operandB[i * 8 + 7 : i * 8], 
                               and_result[i * 8 + 7 : i * 8], 
                                or_result[i * 8 + 7 : i * 8], C[i]
            );
        end
    endgenerate

    assign C[0] = cin;

    // SECOND-LEVEL CARRY LOOKAHEAD
    //region

    //  -- C8

    wire C8_sop_0;

    and AND_C8_0(C8_sop_0, P[0], cin);
    or  OR_C8(C[1], G[0], C8_sop_0);

    //  -- C16

    wire C16_sop_1, C16_sop_0;

    and AND_C16_1(C16_sop_1, P[1], G[0]);
    and AND_C16_0(C16_sop_0, P[1], P[0], cin);
    or  OR_C16(C[2], G[1], C16_sop_1, C16_sop_0);

    //  -- C24

    wire C24_sop_2, C24_sop_1, C24_sop_0;

    and AND_C24_2(C24_sop_2, P[2], G[1]);
    and AND_C24_1(C24_sop_1, P[2], P[1], G[0]);
    and AND_C24_0(C24_sop_0, P[2], P[1], P[0], cin);
    or  OR_C24(C[3], G[2], C24_sop_2, C24_sop_1, C24_sop_0);

    //endregion

endmodule