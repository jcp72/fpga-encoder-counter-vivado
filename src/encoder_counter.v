module encoder_counter(out_count, out_last_index, in_A, in_B, in_INDEX, in_clear);

    // Takes the three input signals from the motor
    // (Encoder A, Encoder B, and Index)
    // and returns:
    //
    //   1) A signed count of quarter-waves encountered
    //      since initialization
    //
    //   2) The last counter value at the time the INDEX
    //      pin had a rising edge

    input in_A, in_B, in_INDEX, in_clear;
    output [31:0] out_count, out_last_index;

    // ENCODER A RISING EDGE COUNTER
    //region

    wire [31:0] encARisingNext, encARisingCurrent, encARisingPlusMinusOne;

    // mux_2(out, select, in)
    mux_2 ENC_A_RISING_MUX(encARisingPlusMinusOne, in_B, {-32'b1, 32'b1});

    // adder_2input(add_result, data_operandA, data_operandB, cin)
    adder_2input ENC_A_RISING_ADD(
        encARisingNext, encARisingCurrent, encARisingPlusMinusOne, 1'b0);

    // reg_32bit(q, d, clk, en, clr)
    reg_32bit ENC_A_RISING_COUNTER(encARisingCurrent, encARisingNext, 
        in_A, 1'b1, in_clear);
    //endregion

    // ENCODER A FALLING EDGE COUNTER
    //region

    wire [31:0] encAFallingNext, encAFallingCurrent, encAFallingPlusMinusOne;

    // mux_2(out, select, in)
    mux_2 ENC_A_FALLING_MUX(encAFallingPlusMinusOne, in_B, {32'b1, -32'b1});

    // adder_2input(add_result, data_operandA, data_operandB, cin)
    adder_2input ENC_A_FALLING_ADD(
        encAFallingNext, encAFallingCurrent, encAFallingPlusMinusOne, 1'b0);

    // reg_32bit(q, d, clk, en, clr)
    reg_32bit ENC_A_FALLING_COUNTER(encAFallingCurrent, encAFallingNext, 
        ~in_A, 1'b1, in_clear);
    //endregion

    // ENCODER B RISING EDGE COUNTER
    //region

    wire [31:0] encBRisingNext, encBRisingCurrent, encBRisingPlusMinusOne;

    // mux_2(out, select, in)
    mux_2 ENC_B_RISING_MUX(encBRisingPlusMinusOne, in_A, {32'b1, -32'b1});

    // adder_2input(add_result, data_operandA, data_operandB, cin)
    adder_2input ENC_B_RISING_ADD(
        encBRisingNext, encBRisingCurrent, encBRisingPlusMinusOne, 1'b0);

    // reg_32bit(q, d, clk, en, clr)
    reg_32bit ENC_B_RISING_COUNTER(encBRisingCurrent, encBRisingNext, 
        in_B, 1'b1, in_clear);
    //endregion

    // ENCODER A FALLING EDGE COUNTER
    //region

    wire [31:0] encBFallingNext, encBFallingCurrent, encBFallingPlusMinusOne;

    // mux_2(out, select, in)
    mux_2 ENC_B_FALLING_MUX(encBFallingPlusMinusOne, in_A, {-32'b1, 32'b1});

    // adder_2input(add_result, data_operandA, data_operandB, cin)
    adder_2input ENC_B_FALLING_ADD(
        encBFallingNext, encBFallingCurrent, encBFallingPlusMinusOne, 1'b0);

    // reg_32bit(q, d, clk, en, clr)
    reg_32bit ENC_B_FALLING_COUNTER(encBFallingCurrent, encBFallingNext, 
        ~in_B, 1'b1, in_clear);
    //endregion

    // FINAL ADDITION STEPS
    wire [31:0] encASum, encBSum;

    // reg_32bit(q, d, clk, en, clr)
    reg_32bit INDEX_REG(out_last_index, out_count, in_INDEX, 1'b1, 1'b0);

    // adder_2input(add_result, data_operandA, data_operandB, cin)
    adder_2input ENC_A_SUM(encASum, encAFallingCurrent, encARisingCurrent, 1'b0);
    adder_2input ENC_B_SUM(encBSum, encBFallingCurrent, encBRisingCurrent, 1'b0);
    adder_2input TOTAL_SUM(out_count, encASum, encBSum, 1'b0);

endmodule