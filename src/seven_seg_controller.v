// Modified from https://web.mit.edu/6.111/volume2/www/f2019/handouts/labs/lab2_19/
module seven_seg_controller(input        clk_in,
                            input        rst_in,
                            input [31:0] val_in,
                            output [7:0] cat_out,
                            output [7:0] an_out
    );
  
    reg [7:0] segment_state = 8'b0000_0001;
    reg [31:0] segment_counter = 32'b0;
    wire [3:0] routed_vals;
    wire [7:0] led_out;
    
    binary_to_seven_segment CONVERTER( .bin_in(routed_vals), .hex_out(led_out));
    assign cat_out = led_out;
    assign an_out = ~segment_state;

    assign routed_vals = (segment_state == 8'b0000_0001) ? val_in[ 3: 0] : 8'bz;
    assign routed_vals = (segment_state == 8'b0000_0010) ? val_in[ 7: 4] : 8'bz;
    assign routed_vals = (segment_state == 8'b0000_0100) ? val_in[11: 8] : 8'bz;
    assign routed_vals = (segment_state == 8'b0000_1000) ? val_in[15:12] : 8'bz;
    assign routed_vals = (segment_state == 8'b0001_0000) ? val_in[19:16] : 8'bz;
    assign routed_vals = (segment_state == 8'b0010_0000) ? val_in[23:20] : 8'bz;
    assign routed_vals = (segment_state == 8'b0100_0000) ? val_in[27:24] : 8'bz;
    assign routed_vals = (segment_state == 8'b1000_0000) ? val_in[31:28] : 8'bz;
    
    always @(posedge clk_in) begin
        if (rst_in) begin
            segment_state <= 8'b0000_0001;
            segment_counter <= 32'b0;
        end else begin
            if (segment_counter == 32'd100_000) begin // clock division to 1 kHz
                segment_counter <= 32'd0;
                segment_state <= {segment_state[6:0], segment_state[7]};
            end else begin
                segment_counter <= segment_counter + 1;
            end
        end
    end
        
endmodule