iverilog -o Counter_TB.vvp -Wimplicit -c file_list/counter_src.txt test/counter_tb.v

if [ $? == 0 ]; then
    vvp Counter_TB.vvp
else 
    echo RUN ABORTED! Your files have one or more error\(s\).
fi
