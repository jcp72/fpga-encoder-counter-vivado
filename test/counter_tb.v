`timescale 1ns/100ps
module counter_tb;

	// Module Inputs
    reg A = 0;
    reg B = 0;
    reg INDEX = 0;
    reg clear = 1;

	// Module outputs
    wire [31:0] out_count, out_last_index;

    // Initialize module
    // encoder_counter(out_count, out_last_index, in_A, in_B, in_INDEX);
    encoder_counter ENCODER_COUNTER(out_count, out_last_index, A, B, INDEX, clear);

	// always begin
    //     #5 
    //     A = ~A;
    //     #5
    //     B = ~B;
    //     // if (clock) begin
    //     //     $display("%b", q);
    //     // end
    // end

    reg [31:0] loopCounter = 0;

    initial begin    
        $dumpfile("counter_tb.vcd"); // Output file name
        $dumpvars(0, counter_tb); // Module to capture and what level, 0 means all wires

        #10;
        clear = 0;
        # 10

        while (loopCounter < 10) begin
            #5 
            A = ~A;
            #5
            B = ~B;
            loopCounter = loopCounter + 1;
        end

        #100
        $finish;
    end
endmodule