from datetime import datetime
from shutil import copy

trailer = input("Enter tail to append on save: ").replace(" ", "_")

copy("EncoderTestV0.runs/impl_1/main.bit","saved_bitfiles/{:%Y%m%d_%H%M%S}_{}.bit".format(datetime.now(), trailer))
