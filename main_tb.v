`timescale 1ns/100ps
module counter_tb;

	// Module Inputs
    reg [15:0] sw = 0;

	// Module outputs
    wire [15:0] led;

    // Instantiate
    main MAIN(sw, led);
    

    initial begin    
        $dumpfile("counter_tb.vcd"); // Output file name
        $dumpvars(0, counter_tb); // Module to capture and what level, 0 means all wires

        while (sw < 10) begin
            #5
            sw = sw + 1;
        end

        #100
        $finish;
    end
endmodule