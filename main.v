module main(input [15:0] SW, output [15:0] LED, input clock, input resetBtn,
	output CA, 
	output CB, 
    output CC,
    output CD,
    output CE,
    output CF,
    output CG,
    output DP,
	output[7:0] AN,
    input encA1,
    input encB1,
    input encZ1,
    output TX);

    wire [31:0] encoderCount;
    wire [31:0] encoderLastIndex;

    // encoder_counter(out_count, out_last_index, in_A, in_B, in_INDEX, in_clear);
    encoder_counter EC(encoderCount, encoderLastIndex, encA1, encB1, encZ1, ~resetBtn);

    // encoder_counter EC(encoderCount, encoderLastIndex, SW[0], SW[1], SW[2], resetBtn);

    ila_0 debuggers(.clk(clock), .probe0(encA1), .probe1(encB1), .probe2(encZ1), .probe3(encoderCount));

    // seven_seg_controller(clk, reset, data, cathodes, anodes)
    seven_seg_controller SSC(clock, 1'b0, encoderCount, {DP, CA, CB, CC, CD, CE, CF, CG}, AN); 

    assign LED = SW;

    // SERIAL COMMUNICATION

    // module inputs
    reg rst = 0;
    reg [7:0] data = 8'd73;
    reg valid = 0;
    reg [15:0] prescale = 16'd217;

    reg [31:0] byteSpacer = 0;

    // STATE EXPLANATION
    /**
     *   Each set of data transmission will be prefixed with a set of four 
     *   zero-bytes.
     *
     *   After the four zero-bytes, there will be 64 bytes transmitted - 
     *   a 4-byte position and a 4-byte zero-crossing for each of 8 motors.
     *
     *   The states then, will look like this:
     *
     *    STATE | DESCRIPTION
     *   -------|-------------
     *      0   | first  0-byte
     *      1   | second 0-byte
     *      2   | third  0-byte
     *      3   | fourth 0-byte
     *
     *      4   | least significant byte of motor 0 position
     *      5   | 2nd-to-least significant byte of motor 0 position
     *      6   | 2nd-to-most significant byte of motor 0 position
     *      7   | most significant byte of motor 0 position
     *
     *      8   | least significant byte of motor 0 zero crossing
     *        ...
     *
     *        ...
     *     67   | most significant byte of motor 7 zero crossing
     *
     */


    // FOR NOW, since there is only one motor, only need states 0-11.

    reg [3:0] state = 0;

    // module outputs
    wire busy, ready;

    uart_tx XMTR(clock, rst, data, valid, ready, TX, busy, prescale);



    always @(posedge clock) begin
        if (~ready) begin
            valid <= 0;
            byteSpacer <= 0;
        end else begin
            if (byteSpacer > 500000) begin // delay = 500,000 * (10 ns clock period) = 5 ms
                // data <= data + 1;
                byteSpacer <= 0;
                
                state <= state + 1;
                valid <= 1;

                if (state == 0) begin
                    data <= 0;
                end else if (state ==  1) begin
                    data <= 0;
                end else if (state ==  2) begin
                    data <= 0;
                end else if (state ==  3) begin
                    data <= 0;

                end else if (state ==  4) begin
                    data <= encoderCount[ 7: 0];
                end else if (state ==  5) begin
                    data <= encoderCount[15: 8];
                end else if (state ==  6) begin
                    data <= encoderCount[23:16];
                end else if (state ==  7) begin
                    data <= encoderCount[31:24];

                end else if (state ==  8) begin
                    data <= encoderLastIndex[ 7: 0];
                end else if (state ==  9) begin
                    data <= encoderLastIndex[15: 8];
                end else if (state == 10) begin
                    data <= encoderLastIndex[23:16];
                end else if (state == 11) begin
                    data <= encoderLastIndex[31:24];

                end else begin
                    data <= 255; // dummy value, fix this later
                end
                
            end else begin 
                byteSpacer <= byteSpacer + 1;
            end
        end
    end

endmodule