void setup() {
  Serial.begin(57600);
}

int state = 0;
unsigned long value = 0;
unsigned long motorPos = 0;
unsigned long zeroCrossing = 0;

long int lastMillisPrint = 0;

void loop() {

  if (Serial.available()) {
    unsigned char c = Serial.read();
//    Serial.println((unsigned int) c);
    if (state < 4) {
      if (c == 0) state++;
      else state = 0;
    } else {
      if (state % 4 == 0) {
        if (state == 8) {
          motorPos = value;
        } else if (state == 12) {
          zeroCrossing = value;
        }
        value = 0;
      }
      value = value | ( ((unsigned long) c) << ((state % 4) * 8) );
      state++;
      state %= 16;
    }
  }

  long int now = millis();
  if (now - lastMillisPrint > 250) {
    // print 4x/second
    Serial.print("ENC: "); 
    Serial.print(motorPos);
    Serial.print(" ZER: ");
    Serial.println(zeroCrossing);
    lastMillisPrint = now;
  }
}
